/*
placar[Steven, Diamante Rosa, velha]
0 - Steve
1 - Diamante Rosa
--------------------------------------------------------------------------------------------------------------
1- Comente para a turma os seus personagens antagonistas
2- Definir personagens  antagonistas e visual harmônico para o tabuleiro (css) - (40 pt)
3- Trocar alert por mensagem de texto no HTML, inclusive quando der empate (velha) (10 pt)
4- Alternador jogador que inicia (seja sempre o perdedor) (10 pt)
5- Melhorar quando se começa a verificar ganhador (a partir da 5 jogada) (10 pt)
6- Melhorar a verificação das linhas para saber se alguém ganhou (reduzir de 8 verificações para o mínimo) (30 pt)
*/


var vez = 0;
var jogada = 0;
var placar = [0,0,0];

function jogar(posicao){
	if ( vez == 0 ) {
		posicao.style.backgroundImage = 'url(img/steven8.png)';
		jogada ++;
		if (jogada >= 5) {
			ganhador(posicao);
			
			if (jogada == 9 && document.getElementById("ganhador").innerHTML == ""){
				document.getElementById("ganhador").innerHTML = "Velha!";
				placar[2] = placar[2] + 1;
				document.getElementById("placar_velha").innerHTML = placar[2];
			}
		}
		vez = 1;

	} else {
		posicao.style.backgroundImage = 'url(img/pinkd3.png)';
		jogada ++;
		if (jogada >= 5) {
			ganhador(posicao);
			
			if (jogada == 9 && document.getElementById("Ganhador").innerHTML == ""){
				document.getElementById("Ganhador").innerHTML = "Velha";
				placar[2] = placar[2] + 1;
				document.getElementById("placar_velha").innerHTML = placar[2];
			}
		}
		vez = 0;
	}
	posicao.onclick = "";

}

function ganhador(posicao){
	var c1 = document.getElementById('c1').style.backgroundImage;
	var c2 = document.getElementById('c2').style.backgroundImage;
	var c3 = document.getElementById('c3').style.backgroundImage;
	var c4 = document.getElementById('c4').style.backgroundImage;
	var c5 = document.getElementById('c5').style.backgroundImage;
	var c6 = document.getElementById('c6').style.backgroundImage;
	var c7 = document.getElementById('c7').style.backgroundImage;
	var c8 = document.getElementById('c8').style.backgroundImage;
	var c9 = document.getElementById('c9').style.backgroundImage;

	
	if (posicao.id == 'c1') {
		if ((c1 == c2 && c2 == c3 && c3!='') ||
		  	(c1 == c4 && c4 == c7 && c7!='') ||
		  	(c1 == c5 && c5 == c9 && c9!='')
			) {
				verificaVez();
				finalizar();
		}
	
	}else if (posicao.id == 'c2') {
		if ((c1 == c2 && c2 == c3 && c3!='') ||
		  	(c2 == c5 && c5 == c8 && c8!='')
			) {
				verificaVez();
				finalizar();
		}
	}else if (posicao.id == 'c3') {
		if ((c1 == c2 && c2 == c3 && c3!='') ||
		  	(c3 == c6 && c6 == c9 && c9!='') ||
		  	(c3 == c5 && c5 == c7 && c7!='') 
			) {
				verificaVez();
				finalizar();
		}
	}else if (posicao.id == 'c4') {
		if ((c4 == c5 && c5 == c6 && c6!='') ||
		  	(c1 == c4 && c4 == c7 && c7!='')
			) {
				verificaVez();
				finalizar();
		}
	}else if (posicao.id == 'c5') {
		if ((c4 == c5 && c5 == c6 && c6!='') ||
		  	(c2 == c5 && c5 == c8 && c8!='') ||
		  	(c1 == c5 && c5 == c9 && c9!='') ||
		 	(c3 == c5 && c5 == c7 && c7!='') 
			) {
				verificaVez();
				finalizar();
		}
	}else if (posicao.id == 'c6') {
		if ((c4 == c5 && c5 == c6 && c6!='') ||
		  	(c3 == c6 && c6 == c9 && c9!='')
			) {
				verificaVez();
				finalizar();
		}
	}else if (posicao.id == 'c7') {
		if ((c7 == c8 && c8 == c9 && c9!='') ||
		  	(c1 == c4 && c4 == c7 && c7!='') ||
		  	(c3 == c5 && c5 == c7 && c7!='') 
			) {
				verificaVez();
				finalizar();
		}
	}else if (posicao.id == 'c8') {
		if ((c7 == c8 && c8 == c9 && c9!='') ||
		  	(c2 == c5 && c5 == c8 && c8!='')
			) {
				verificaVez();
				finalizar();
		}
	}else if (posicao.id == 'c9') {
		if ((c7 == c8 && c8 == c9 && c9!='') ||
		  	(c3 == c6 && c6 == c9 && c9!='') ||
		  	(c1 == c5 && c5 == c9 && c9!='')
			) {
				verificaVez();
				finalizar();
		}
	}
}


	


function verificaVez(){
	if (vez == 0){
			document.getElementById("ganhador").innerHTML = "Steven ganhou!";
			vez = 1;
			placar[0] = placar[0] + 1;
			document.getElementById("placar_steven").innerHTML = placar[0];
		}else{
			document.getElementById("ganhador").innerHTML = "Diamante Rosa ganhou!";
			vez = 0;
			placar[1] = placar[1] + 1;
			document.getElementById("placar_diamante").innerHTML = placar[1];
		}
}

function finalizar(){
	for (var i = 1; i <= 9 ; i++) {
		document.getElementById('c'+i).onclick = "";
	}
	jogada = 0;


}

function reiniciar(){
	for (var i = 1; i <= 9 ; i++) {
		document.getElementById('c' + i).setAttribute("onclick", "jogar(c" + i + ")");
		document.getElementById('c' + i).removeAttribute("style");
	}
	document.getElementById("ganhador").innerHTML = "";

}
